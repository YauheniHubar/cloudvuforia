//
//  ViewController.swift
//  VuforiaSample

import UIKit

class ViewController: UIViewController, UIPopoverPresentationControllerDelegate {
    
    var vuforiaManager: VuforiaManager? = nil
    
    let boxMaterial = SCNMaterial()
    
    var avc:AdViewController? = nil
    
    var isAdVisible = false
    
    var searchresult:SearchResult? = nil
    
    let scene = SCNScene()
    
    var trackable:VuforiaTrackable? = nil
    
    fileprivate var lastSceneName: String? = nil
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepare()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        do {
//            try vuforiaManager?.stop()
//        }catch let error {
//            print("\(error)")
//        }
    }
    
    func presentAd() {
        self.searchresult = vuforiaManager?.searchResult
        if !isAdVisible && (self.searchresult != nil) {
            avc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ad") as? AdViewController
            avc?.searchResult = self.searchresult
            avc?.modalPresentationStyle = .popover
            avc?.popoverPresentationController?.delegate = self
            avc?.preferredContentSize = CGSize(width: 200.0, height: 250.0)
            let pp = (avc?.popoverPresentationController!)! as UIPopoverPresentationController
            pp.backgroundColor = UIColor .clear
            pp.sourceView = self.view
            pp.sourceRect = CGRect .zero
            self.present(avc!, animated: true, completion: nil)
            isAdVisible = true
        }
    }
    
    func dismissAd() {
        avc?.dismiss(animated: true, completion: nil)
        searchresult = nil
        isAdVisible = false
    }

    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController!) -> UIModalPresentationStyle {
        // Return no adaptive presentation style, use default presentation behaviour
        return .none
    }
    
    func switchViewController() {
        vuforiaManager?.isSwitchingControllers = true;
        do {
            try vuforiaManager?.stop()
        }catch let error {
            print("\(error)")
        }
    }
    
    func proceedSwitching() {
        self.performSegue(withIdentifier: "switch", sender: self)
    }
    
}

private extension ViewController {
    func prepare() {
        let prefs = UserDefaults.standard
        let vuforiaLicenseKey:String = prefs.value(forKey: "vuforiaLicenseKey") as! String
        vuforiaManager = VuforiaManager(licenseKey: vuforiaLicenseKey, dataSetFile: nil, viewController:self)
        if let manager = vuforiaManager {
            manager.delegate = self
            manager.eaglView.sceneSource = self
            manager.eaglView.delegate = self
            manager.eaglView.setupRenderer()
            self.view = manager.eaglView
        }
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(didRecieveWillResignActiveNotification),
                                       name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        
        notificationCenter.addObserver(self, selector: #selector(didRecieveDidBecomeActiveNotification),
                                       name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        vuforiaManager?.prepare(with: .portrait)
    }
    
    func pause() {
        do {
            try vuforiaManager?.pause()
        }catch let error {
            print("\(error)")
        }
    }
    
    func resume() {
        do {
            try vuforiaManager?.resume()
        }catch let error {
            print("\(error)")
        }
    }
}

extension ViewController {
    func didRecieveWillResignActiveNotification(_ notification: Notification) {
        pause()
    }
    
    func didRecieveDidBecomeActiveNotification(_ notification: Notification) {
        resume()
    }
}

extension ViewController: VuforiaManagerDelegate {
    func vuforiaManagerDidFinishPreparing(_ manager: VuforiaManager!) {
        print("did finish preparing\n")
        
        do {
            try vuforiaManager?.start()
            vuforiaManager?.setContinuousAutofocusEnabled(true)
        }catch let error {
            print("\(error)")
        }
        do {
            try vuforiaManager?.pause()
        } catch let error {
            print("\(error)")
        }
        do {
            try vuforiaManager?.resume()
        } catch let error {
            print ("\(error)")
        }
    }
    
    func vuforiaManager(_ manager: VuforiaManager!, didFailToPreparingWithError error: Error!) {
        print("Cloud: did fail to preparing \(error)\n")
    }
    
    func vuforiaManager(_ manager: VuforiaManager!, didUpdateWith state: VuforiaState!) {
        self.searchresult = vuforiaManager?.searchResult
        trackable = state.trackable(at: 0)
        if self.searchresult?.targetName == "whiteTea" {
            boxMaterial.diffuse.contents = UIColor.red
            
            if lastSceneName != "whiteBox" {
                manager.eaglView.setNeedsChangeSceneWithUserInfo(["scene" : "whiteBox"])
                lastSceneName = "whiteBox"
                presentAd()
            }
        }else {
            boxMaterial.diffuse.contents = UIColor.blue
            
            if lastSceneName != "redBox" {
                manager.eaglView.setNeedsChangeSceneWithUserInfo(["scene" : "redBox"])
                lastSceneName = "redBox"
                presentAd()
            }
        }
    }
}

extension ViewController: VuforiaEAGLViewSceneSource, VuforiaEAGLViewDelegate {
    
    func scene(for view: VuforiaEAGLView!, userInfo: [String : Any]?) -> SCNScene! {
        guard let userInfo = userInfo else {
            print("default scene")
//            return createWhiteBoxScene(with: view)
            return scene
        }
        
        if let sceneName = userInfo["scene"] as? String , sceneName == "whiteBox" {
            print("whiteBox scene")
            return createWhiteBoxScene(with: view)
        }else {
            print("redBox scene")
            return createRedBoxScene(with: view)
        }
        
    }
    
    fileprivate func createWhiteBoxScene(with view: VuforiaEAGLView) -> SCNScene {
        
        boxMaterial.diffuse.contents = UIColor.lightGray
        
        for node in scene.rootNode.childNodes {
            node.removeFromParentNode()
        }
        
        let lightNode = SCNNode()
        lightNode.light = SCNLight()
        lightNode.light?.type = .omni
        lightNode.light?.color = UIColor.lightGray
        lightNode.position = SCNVector3(x:0, y:10, z:10)
        scene.rootNode.addChildNode(lightNode)
        
        let ambientLightNode = SCNNode()
        ambientLightNode.light = SCNLight()
        ambientLightNode.light?.type = .ambient
        ambientLightNode.light?.color = UIColor.darkGray
        scene.rootNode.addChildNode(ambientLightNode)
        
        let whiteBox = SCNNode()
        whiteBox.name = "whiteBox"
        whiteBox.geometry = SCNBox(width: 2.3, height: 1.2, length: 1.2, chamferRadius: 0.05)
        for i in stride(from: 0, to: 5, by: 1) {
            let mat = SCNMaterial()
            let sideImage = UIImage(named: "hw\(i).jpg")
            mat.diffuse.contents = sideImage
            var mats = (whiteBox.geometry?.materials)!
            mats.append(mat)
            whiteBox.geometry?.materials = mats
        }
        whiteBox.position = SCNVector3Make(0.0, 0.0, -2.0);
        whiteBox.scale = SCNVector3Make(1.7, 1.7, 1.7)
        let xAngle = SCNMatrix4MakeRotation(0, 0, 0, 0)
        let yAngle = SCNMatrix4MakeRotation(Float(M_PI), 0, 1, 0)
        let zAngle = SCNMatrix4MakeRotation(Float(-M_PI_2), 0, 0, 1)
        
        let rotationMatrix = SCNMatrix4Mult(SCNMatrix4Mult(xAngle, yAngle), zAngle)
        
        whiteBox.transform = SCNMatrix4Mult(rotationMatrix, whiteBox.transform)
        scene.rootNode.addChildNode(whiteBox)
        
        return scene
    }
    
    fileprivate func createRedBoxScene(with view: VuforiaEAGLView) -> SCNScene {
        
        boxMaterial.diffuse.contents = UIColor.lightGray
        
        for node in scene.rootNode.childNodes {
            node.removeFromParentNode()
        }
        
        let lightNode = SCNNode()
        lightNode.light = SCNLight()
        lightNode.light?.type = .omni
        lightNode.light?.color = UIColor.lightGray
        lightNode.position = SCNVector3(x:0, y:10, z:10)
        scene.rootNode.addChildNode(lightNode)
        
        let ambientLightNode = SCNNode()
        ambientLightNode.light = SCNLight()
        ambientLightNode.light?.type = .ambient
        ambientLightNode.light?.color = UIColor.darkGray
        scene.rootNode.addChildNode(ambientLightNode)
        
        let redBox = SCNNode()
        redBox.name = "redBox"
        redBox.geometry = SCNBox(width: 2.9, height: 1.3, length: 0.85, chamferRadius: 0.05)
        for i in stride(from: 0, to: 5, by: 1) {
            let mat = SCNMaterial()
            let sideImage = UIImage(named: "hr\(i).jpg")
            mat.diffuse.contents = sideImage
            var mats = (redBox.geometry?.materials)!
            mats.append(mat)
            redBox.geometry?.materials = mats
        }
        redBox.position = SCNVector3Make(0.0, 0.0, -2.0);
        redBox.scale = SCNVector3Make(1.7, 1.7, 1.7)
        let xAngle = SCNMatrix4MakeRotation(0, 0, 0, 0)
        let yAngle = SCNMatrix4MakeRotation(Float(M_PI), 0, 1, 0)
        let zAngle = SCNMatrix4MakeRotation(Float(-M_PI_2), 0, 0, 1)
        
        let rotationMatrix = SCNMatrix4Mult(SCNMatrix4Mult(xAngle, yAngle), zAngle)
        
        redBox.transform = SCNMatrix4Mult(rotationMatrix, redBox.transform)
        scene.rootNode.addChildNode(redBox)
        
        return scene
    }
    
    func vuforiaEAGLView(_ view: VuforiaEAGLView!, didTouchDownNode node: SCNNode!) {
        print("touch down \(node.name)\n")
        boxMaterial.transparency = 0.6
    }
    
    func vuforiaEAGLView(_ view: VuforiaEAGLView!, didTouchUp node: SCNNode!) {
        print("touch up \(node.name)\n")
        boxMaterial.transparency = 1.0
    }
    
    func vuforiaEAGLView(_ view: VuforiaEAGLView!, didTouchCancel node: SCNNode!) {
        print("touch cancel \(node.name)\n")
        boxMaterial.transparency = 1.0
    }
}

