//
//  AdViewController.swift
//  VuforiaSampleSwift

import UIKit

class AdViewController: UIViewController {

    @IBOutlet weak var teaLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    public var searchResult:SearchResult?
    
    required init?(coder aDecoder: NSCoder) {
        searchResult = nil
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        teaLbl.text = searchResult?.targetName as String?
        priceLbl.text = searchResult?.metadata as String?
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
