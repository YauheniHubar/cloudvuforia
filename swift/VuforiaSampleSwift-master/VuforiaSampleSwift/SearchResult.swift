//
//  SearchResult.swift
//  VuforiaSampleSwift
//
//  Created by Yauheni Hubar on 11/8/16.
//  Copyright © 2016 Yoshihiro Kato. All rights reserved.
//

import Foundation

@objc class SearchResult:NSObject {
    
    var targetName:NSString = ""
    var uniqueTargetId:NSString = ""
    var targetSize:NSNumber = 0.0
    var metadata:NSString = ""
    var trackingRating:NSNumber = 0.0
    
}
