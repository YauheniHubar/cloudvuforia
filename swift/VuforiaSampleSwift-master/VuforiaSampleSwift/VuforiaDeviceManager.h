//
//  VuforiaDeviceManager.h
//  VuforiaSampleSwift
//
//  Created by Yauheni Hubar on 11/9/16.
//  Copyright © 2016 Yoshihiro Kato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "VuforiaObjects.h"

@class SearchResult;
@class DeviceViewController;
@class VuforiaDeviceManager;
@class VuforiaDeviceEAGLView;

@protocol VuforiaDeviceManagerDelegate <NSObject>

- (void)vuforiaDeviceManagerDidFinishPreparing:(VuforiaDeviceManager*) manager;
- (void)vuforiaDeviceManager:(VuforiaDeviceManager*)manager didFailToPreparingWithError:(NSError*)error;
- (void)vuforiaDeviceManager:(VuforiaDeviceManager *)manager didUpdateWithState:(VuforiaState*)state;

@end

@interface VuforiaDeviceManager : NSObject

@property (nonatomic, weak) id<VuforiaDeviceManagerDelegate> delegate;
@property (nonatomic, readonly)BOOL isRetinaDisplay;
@property (nonatomic, readonly)BOOL extendedTrackingEnabled;
@property (nonatomic, readonly)BOOL continuousAutofocusEnabled;
@property (nonatomic, readonly)BOOL flashEnabled;
@property (nonatomic, readonly)BOOL frontCameraEnabled;
@property (nonatomic, readonly)CGRect viewport;
@property (nonatomic, readonly)VuforiaDeviceEAGLView* eaglView;
@property (nonatomic, strong) SearchResult * searchResult;
@property (nonatomic) BOOL isSwitchingControllers;

- (instancetype)init __attribute__((unavailable("init is not available")));
- (instancetype)initWithLicenseKey:(NSString*)licenseKey dataSetFile:(NSString*)path viewController:(DeviceViewController *)viewController;

- (CGSize)preferredARFrameSize;


- (void)prepareWithOrientation:(UIInterfaceOrientation)orientation;

- (BOOL)setExtendedTrackingEnabled:(BOOL)enabled;
- (BOOL)setContinuousAutofocusEnabled:(BOOL)enabled;
- (BOOL)setFlashEnabled:(BOOL)enabled;
- (BOOL)setFrontCameraEnabled:(BOOL)enabled;

- (BOOL)resume:(NSError **)error;
- (BOOL)pause:(NSError **)error;

- (BOOL)start:(NSError **)error;
- (BOOL)stop:(NSError **)error;

@end
