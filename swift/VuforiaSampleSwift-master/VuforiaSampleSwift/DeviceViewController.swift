//
//  DeviceViewController.swift
//  VuforiaSampleSwift

import UIKit

class DeviceViewController: UIViewController {
    
    let vuforiaDataSetFile = "vuforiadevice.xml"
    
    let scene = SCNScene()
    
    var vuforiaDeviceManager: VuforiaDeviceManager? = nil
    
    let boxMaterial = SCNMaterial()
    fileprivate var lastSceneName: String? = nil
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepare()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func switchViewController() {
        vuforiaDeviceManager?.isSwitchingControllers = true;
        do {
            try vuforiaDeviceManager?.stop()
        }catch let error {
            print("\(error)")
        }
    }
    
    func proceedSwitching() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "cloud")
        self.present(controller, animated: false, completion: nil)
    }

}

private extension DeviceViewController {
    func prepare() {
        let prefs = UserDefaults.standard
        let vuforiaLicenseKey:String = prefs.value(forKey: "vuforiaLicenseKey") as! String
        vuforiaDeviceManager = VuforiaDeviceManager(licenseKey: vuforiaLicenseKey, dataSetFile: vuforiaDataSetFile, viewController:self)
        if let manager = vuforiaDeviceManager {
            manager.delegate = self
            manager.eaglView.sceneSource = self
            manager.eaglView.delegate = self
            manager.eaglView.setupRenderer()
            self.view = manager.eaglView
        }
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(didRecieveWillResignActiveNotification),
                                       name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        
        notificationCenter.addObserver(self, selector: #selector(didRecieveDidBecomeActiveNotification),
                                       name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        vuforiaDeviceManager?.prepare(with: .portrait)
    }
    
    func pause() {
        do {
            try vuforiaDeviceManager?.pause()
        }catch let error {
            print("\(error)")
        }
    }
    
    func resume() {
        do {
            try vuforiaDeviceManager?.resume()
        }catch let error {
            print("\(error)")
        }
    }
}

extension DeviceViewController {
    func didRecieveWillResignActiveNotification(_ notification: Notification) {
        pause()
    }
    
    func didRecieveDidBecomeActiveNotification(_ notification: Notification) {
        resume()
    }
}

extension DeviceViewController: VuforiaDeviceManagerDelegate {
    func vuforiaDeviceManagerDidFinishPreparing(_ manager: VuforiaDeviceManager!) {
        print("did finish preparing\n")
        
        do {
            try vuforiaDeviceManager?.start()
            vuforiaDeviceManager?.setContinuousAutofocusEnabled(true)
        }catch let error {
            print("\(error)")
        }
        do {
            try vuforiaDeviceManager?.pause()
        } catch let error {
            print("\(error)")
        }
        do {
            try vuforiaDeviceManager?.resume()
        } catch let error {
            print ("\(error)")
        }
    }
    
    func vuforiaDeviceManager(_ manager: VuforiaDeviceManager!, didFailToPreparingWithError error: Error!) {
        print("Device: did fail to preparing \(error)\n")
    }
    
    func vuforiaDeviceManager(_ manager: VuforiaDeviceManager!, didUpdateWith state: VuforiaState!) {
        for index in 0 ..< state.numberOfTrackableResults {
            let result = state.trackableResult(at: index)
            let trackerableName = result?.trackable.name
            //print("\(trackerableName)")
            if trackerableName == "stones" {
                boxMaterial.diffuse.contents = UIColor.red
                
                if lastSceneName != "stones" {
                    manager.eaglView.setNeedsChangeSceneWithUserInfo(["scene" : "stones"])
                    lastSceneName = "stones"
                }
            }else {
                boxMaterial.diffuse.contents = UIColor.blue
                
                if lastSceneName != "chips" {
                    manager.eaglView.setNeedsChangeSceneWithUserInfo(["scene" : "chips"])
                    lastSceneName = "chips"
                }
            }
        }
    }
}

extension DeviceViewController: VuforiaDeviceEAGLViewSceneSource, VuforiaDeviceEAGLViewDelegate {
    
    func scene(for view: VuforiaDeviceEAGLView!, userInfo: [String : Any]?) -> SCNScene! {
        return createChocolateScene(with: view)
    }
    
    fileprivate func createChocolateScene(with view: VuforiaDeviceEAGLView) -> SCNScene {
        
        boxMaterial.diffuse.contents = UIColor.lightGray
        
        let lightNode = SCNNode()
        lightNode.light = SCNLight()
        lightNode.light?.type = .omni
        lightNode.light?.color = UIColor.darkGray
        lightNode.position = SCNVector3(x:0, y:10, z:10)
        scene.rootNode.addChildNode(lightNode)
        
        let ambientLightNode = SCNNode()
        ambientLightNode.light = SCNLight()
        ambientLightNode.light?.type = .ambient
        ambientLightNode.light?.color = UIColor.darkGray
        scene.rootNode.addChildNode(ambientLightNode)
        
        let shoBox = SCNNode()
        shoBox.name = "shoBox"
        shoBox.geometry = SCNBox(width: 1.35, height: 2.75, length: 0.25, chamferRadius: 0.05)
        for i in stride(from: 0, to: 5, by: 1) {
            let mat = SCNMaterial()
            let sideImage = UIImage(named: "sho\(i).jpg")
            mat.diffuse.contents = sideImage
            var mats = (shoBox.geometry?.materials)!
            mats.append(mat)
            shoBox.geometry?.materials = mats
        }
        shoBox.position = SCNVector3Make(0.0, 0.0, -2.0);
        shoBox.scale = SCNVector3Make(1.7, 1.7, 1.7)
        let xAngle = SCNMatrix4MakeRotation(0, 0, 0, 0)
        let yAngle = SCNMatrix4MakeRotation(Float(M_PI), 0, 1, 0)
        let zAngle = SCNMatrix4MakeRotation(Float(-M_PI_2), 0, 0, 0)
        
        let rotationMatrix = SCNMatrix4Mult(SCNMatrix4Mult(xAngle, yAngle), zAngle)
        
        shoBox.transform = SCNMatrix4Mult(rotationMatrix, shoBox.transform)
        scene.rootNode.addChildNode(shoBox)
        
        return scene
    }
    
    func vuforiaDeviceEAGLView(_ view: VuforiaDeviceEAGLView!, didTouchDownNode node: SCNNode!) {
        print("touch down \(node.name)\n")
        boxMaterial.transparency = 0.6
    }
    
    func vuforiaDeviceEAGLView(_ view: VuforiaDeviceEAGLView!, didTouchUp node: SCNNode!) {
        print("touch up \(node.name)\n")
        boxMaterial.transparency = 1.0
    }
    
    func vuforiaDeviceEAGLView(_ view: VuforiaDeviceEAGLView!, didTouchCancel node: SCNNode!) {
        print("touch cancel \(node.name)\n")
        boxMaterial.transparency = 1.0
    }
}

