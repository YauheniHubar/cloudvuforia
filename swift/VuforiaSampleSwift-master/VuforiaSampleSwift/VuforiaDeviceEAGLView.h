//
//  VuforiaDeviceEAGLView.h
//  VuforiaSampleSwift
//
//  Created by Yauheni Hubar on 11/9/16.
//  Copyright © 2016 Yoshihiro Kato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SceneKit/SceneKit.h>
#import <Vuforia/UIGLViewProtocol.h>

@class SearchResult;
@class DeviceViewController;
@class VuforiaDeviceEAGLView;
@class VuforiaDeviceManager;

@protocol VuforiaDeviceEAGLViewSceneSource <NSObject>

- (SCNScene *)sceneForEAGLView:(VuforiaDeviceEAGLView *)view userInfo:(NSDictionary<NSString*, id>*)userInfo;

@end

@protocol VuforiaDeviceEAGLViewDelegate <NSObject>

- (void)vuforiaDeviceEAGLView:(VuforiaDeviceEAGLView*)view didTouchDownNode:(SCNNode *)node;
- (void)vuforiaDeviceEAGLView:(VuforiaDeviceEAGLView*)view didTouchUpNode:(SCNNode *)node;
- (void)vuforiaDeviceEAGLView:(VuforiaDeviceEAGLView*)view didTouchCancelNode:(SCNNode *)node;

@end

@interface VuforiaDeviceEAGLView : UIView <UIGLViewProtocol>

@property (weak, nonatomic)id<VuforiaDeviceEAGLViewSceneSource> sceneSource;
@property (weak, nonatomic)id<VuforiaDeviceEAGLViewDelegate> delegate;
@property (nonatomic, assign)CGFloat objectScale;
@property (nonatomic, strong) SearchResult * searchResult;
@property (nonatomic, strong) DeviceViewController * vc;

- (id)initWithFrame:(CGRect)frame manager:(VuforiaDeviceManager *) deviceManager;

- (void)setupRenderer;
- (void)setNeedsChangeSceneWithUserInfo: (NSDictionary*)userInfo;

- (void)finishOpenGLESCommands;
- (void)freeOpenGLESResources;

- (void)setOffTargetTrackingMode:(BOOL) enabled;

@end
