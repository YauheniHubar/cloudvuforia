//
//  AppDelegate.swift
//  VuforiaSample
//
//  Created by Yoshihiro Kato on 2016/07/02.
//  Copyright © 2016年 Yoshihiro Kato. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let prefs = UserDefaults.standard
        prefs.setValue("AVs/OKr/////AAAAGU8PkqOnY0U3hXgQixEMgzgcETcEnMj7HRCapLGaI5HNvP81KZloDsZGuElouQYK3mrEAN684h9rvXnLesmRiq+g2XHBxwXu50iniEJl/pKQ6FOYX1wlT/u7TeoITwOS3Nzi7s4mIFp0zYMPbuh3ERLFuiEAP0FCijcERocAULEa+zUMJGRAFyzdxdWZPG/BbCgiqTsAhDfwQmbdgJUosm+pL8wpXDNxK6Me3q4cf3jaoV5Z0MhzqSNyfMve0xr+BLTmDt/jnAfjWvFKt+KvQQs9lFqUXR6uea4QIEP/UKM/VRvwyYKzna64cdS8VxEJTOD5iwQSo9G8+F8K57pWd60GYiiISh3TBMMLVfawqP5h", forKey: "vuforiaLicenseKey")
        prefs.setValue("4a5f97e46e21a23542d3c6c5c2616a9d69338922", forKey: "accessKey")
        prefs.setValue("0e43614598cba3beae0b2925baa06511d43f10ca", forKey: "secretKey")
        prefs.synchronize()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

