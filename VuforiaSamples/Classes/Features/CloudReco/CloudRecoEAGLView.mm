/*===============================================================================
Copyright (c) 2016 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/



#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import <sys/time.h>

#import <Vuforia/Vuforia.h>
#import <Vuforia/State.h>
#import <Vuforia/Tool.h>
#import <Vuforia/Renderer.h>
#import <Vuforia/VideoBackgroundConfig.h>
#import <Vuforia/TrackableResult.h>
#import <Vuforia/ImageTargetResult.h>
#import <Vuforia/TrackerManager.h>
#import <Vuforia/TargetFinder.h>

#import "CloudRecoEAGLView.h"
#import "Texture.h"
#import "SampleApplicationUtils.h"
#import "SampleApplicationShaderUtils.h"
#import "Teapot.h"
#import "CloudRecoViewController.h"
#import "Scene.h"
#import "SearchResult.h"

//******************************************************************************
// *** OpenGL ES thread safety ***
//
// OpenGL ES on iOS is not thread safe.  We ensure thread safety by following
// this procedure:
// 1) Create the OpenGL ES context on the main thread.
// 2) Start the Vuforia camera, which causes Vuforia to locate our EAGLView and start
//    the render thread.
// 3) Vuforia calls our renderFrameVuforia method periodically on the render thread.
//    The first time this happens, the defaultFramebuffer does not exist, so it
//    is created with a call to createFramebuffer.  createFramebuffer is called
//    on the main thread in order to safely allocate the OpenGL ES storage,
//    which is shared with the drawable layer.  The render (background) thread
//    is blocked during the call to createFramebuffer, thus ensuring no
//    concurrent use of the OpenGL ES context.
//
//******************************************************************************



namespace {
    // --- Data private to this unit ---
    
    // Model scale factor
    const float kObjectScale = 3.0f;
}

@implementation Object3D

@synthesize numVertices;
@synthesize vertices;
@synthesize normals;
@synthesize texCoords;
@synthesize numIndices;
@synthesize indices;
@synthesize texture;

@end

@interface CloudRecoEAGLView (PrivateMethods)

//@property (strong, nonatomic) Scene * myScene;

- (void)initShaders;
- (void)createFramebuffer;
- (void)deleteFramebuffer;
- (void)setFramebuffer;
- (BOOL)presentFramebuffer;

@end


@implementation CloudRecoEAGLView

@synthesize vapp;
@synthesize viewController;
@synthesize scnView;

SCNMatrix4 inverted;
Scene * myScene;
SCNNode * redBox;
SCNNode * whiteBox;
SCNNode * selectedBox;
SCNNode * boxNode;

// You must implement this method, which ensures the view's underlying layer is
// of type CAEAGLLayer
+ (Class)layerClass
{
    return [CAEAGLLayer class];
}

//------------------------------------------------------------------------------
#pragma mark - Lifecycle

- (id)initWithFrame:(CGRect)frame  appSession:(SampleApplicationSession *) app viewController:(CloudRecoViewController *) aViewController
{
    self = [super initWithFrame:frame];
    
    if (self) {
        vapp = app;
        viewController = aViewController;
        
        // Enable retina mode if available on this device
        if (YES == [vapp isRetinaDisplay]) {
            [self setContentScaleFactor:[UIScreen mainScreen].nativeScale];
        }
        
        // Create the OpenGL ES context
        context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        
        // The EAGLContext must be set for each thread that wishes to use it.
        // Set it the first time this method is called (on the main thread)
        if (context != [EAGLContext currentContext]) {
            [EAGLContext setCurrentContext:context];
        }
        
        sampleAppRenderer = [[SampleAppRenderer alloc] initWithSampleAppRendererControl:self deviceMode:Vuforia::Device::MODE_AR stereo:false nearPlane:1 farPlane:5000];
        
        offTargetTrackingEnabled = NO;

        [self initShaders];
        
        scnView = [[SCNView alloc] initWithFrame:frame options:nil];
        SCNScene * scene = [SCNScene new];
        [scnView setScene:scene];
        [scnView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:scnView];
        
        Scene * sceneSource = [[Scene alloc] initScene];
        [self setSceneSource:sceneSource];
        
        [sampleAppRenderer initRendering];
        [self setupRenderer];
        
//        boxNode = [SCNNode new];
        redBox = [SCNNode nodeWithGeometry:[SCNBox boxWithWidth:0.5 height:0.4 / 1.58 length:0.35 / 1.58 chamferRadius:0.05]];
        for (int i = 0; i < 5; i++) {
            SCNMaterial * mat = [SCNMaterial new];
            UIImage * sideImage = [UIImage imageNamed:[NSString stringWithFormat:@"hr%d.jpg", i]];
            [[mat diffuse] setContents:sideImage];
            NSMutableArray<SCNMaterial *> * mats = [[[redBox geometry] materials] mutableCopy];
            [mats addObject:mat];
            [[redBox geometry] setMaterials:mats];
        }
        
        [redBox setPosition:SCNVector3Make(0, 0, 10)];
        
        whiteBox = [SCNNode nodeWithGeometry:[SCNBox boxWithWidth:0.5 height:0.4 / 1.58 length:0.35 / 1.58 chamferRadius:0.05]];
        for (int i = 0; i < 5; i++) {
            SCNMaterial * mat = [SCNMaterial new];
            UIImage * sideImage = [UIImage imageNamed:[NSString stringWithFormat:@"hw%d.jpg", i]];
            [[mat diffuse] setContents:sideImage];
            NSMutableArray<SCNMaterial *> * mats = [[[whiteBox geometry] materials] mutableCopy];
            [mats addObject:mat];
            [[whiteBox geometry] setMaterials:mats];
        }
        
        [whiteBox setPosition:SCNVector3Make(0, 0, 10)];
        selectedBox = [SCNNode new];
//        [boxNode addChildNode:box];

    }
    
    return self;
}

- (void)setupRenderer {
    self.renderer = [SCNRenderer rendererWithContext:context options:nil];
    self.renderer.autoenablesDefaultLighting = YES;
    self.renderer.playing = YES;
    
    if (self.sceneSource != nil) {
        self.renderer.scene = [self.sceneSource sceneForEAGLView:self];
        
        SCNCamera *camera = [SCNCamera camera];
        self.cameraNode = [SCNNode node];
        self.cameraNode.camera = camera;
        [self.renderer.scene.rootNode addChildNode:self.cameraNode];
        self.renderer.pointOfView = self.cameraNode;
    }
}

// Converts Vuforia matrix to SceneKit matrix
- (SCNMatrix4)SCNMatrix4FromQCARMatrix44:(Vuforia::Matrix44F)matrix {
    GLKMatrix4 glkMatrix;
    for(int i=0; i<16; i++) {
        glkMatrix.m[i] = matrix.data[i];
    }
    return SCNMatrix4FromGLKMatrix4(glkMatrix);
}

// Calculate inverse matrix and assign it to cameraNode
- (void)setCameraMatrix:(Vuforia::Matrix44F)matrix {
    SCNMatrix4 extrinsic = [self SCNMatrix4FromQCARMatrix44:matrix];
    inverted = SCNMatrix4Invert(extrinsic); // inverse matrix!
    NSLog(@"Inverted matrix1: - [%f %f %f %f]", inverted.m11, inverted.m12, inverted.m13, inverted.m14);
    NSLog(@"Inverted matrix2: - [%f %f %f %f]", inverted.m21, inverted.m22, inverted.m23, inverted.m24);
    NSLog(@"Inverted matrix3: - [%f %f %f %f]", inverted.m31, inverted.m32, inverted.m33, inverted.m34);
    NSLog(@"Inverted matrix4: - [%f %f %f %f]", inverted.m41, inverted.m42, inverted.m43, inverted.m44);
    NSLog(@" ");
    self.cameraNode.transform = inverted; // assign it to the camera node's transform property.
}

- (void)setProjectionMatrix:(Vuforia::Matrix44F)matrix {
    self.cameraNode.camera.projectionTransform = [self SCNMatrix4FromQCARMatrix44:matrix];
}


- (void)dealloc
{
    [self deleteFramebuffer];
    
    // Tear down context
    if ([EAGLContext currentContext] == context) {
        [EAGLContext setCurrentContext:nil];
    }
}


- (void)finishOpenGLESCommands
{
    // Called in response to applicationWillResignActive.  The render loop has
    // been stopped, so we now make sure all OpenGL ES commands complete before
    // we (potentially) go into the background
    if (context) {
        [EAGLContext setCurrentContext:context];
        glFinish();
    }
}


- (void)freeOpenGLESResources
{
    // Called in response to applicationDidEnterBackground.  Free easily
    // recreated OpenGL ES resources
    [self deleteFramebuffer];
    glFinish();
}

- (void) setOffTargetTrackingMode:(BOOL) enabled {
    offTargetTrackingEnabled = enabled;
}


- (void) updateRenderingPrimitives
{
    [sampleAppRenderer updateRenderingPrimitives];
}


//------------------------------------------------------------------------------
#pragma mark - UIGLViewProtocol methods

// Draw the current frame using OpenGL
//
// This method is called by Vuforia when it wishes to render the current frame to
// the screen.
//
// *** Vuforia will call this method periodically on a background thread ***
- (void)renderFrameVuforia
{
    if (! vapp.cameraIsStarted) {
        return;
    }
    
    [sampleAppRenderer renderFrameVuforia];
}


- (void)renderFrameWithState:(const Vuforia::State &)state projectMatrix:(Vuforia::Matrix44F &)projectionMatrix
{
    [self setFramebuffer];
    
    // Clear colour and depth buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    // Render video background
    [sampleAppRenderer renderVideoBackground];
    
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    
    [self setProjectionMatrix:projectionMatrix];
    
    if (!state.getNumTrackableResults()) {
        [selectedBox removeFromParentNode];
        [viewController dismissAd];
    }

    for (int i = 0; i < state.getNumTrackableResults(); ++i) {
        // Get the trackable
        const Vuforia::TrackableResult* result = state.getTrackableResult(i);
        Vuforia::Matrix44F modelViewMatrix = Vuforia::Tool::convertPose2GLMatrix(result->getPose());
        
        // OpenGL 2
        Vuforia::Matrix44F modelViewProjection;
        
        SampleApplicationUtils::translatePoseMatrix(0.0f, 0.0f, kObjectScale, &modelViewMatrix.data[0]);
        SampleApplicationUtils::scalePoseMatrix(kObjectScale, kObjectScale, kObjectScale, &modelViewMatrix.data[0]);
        SampleApplicationUtils::multiplyMatrix(&projectionMatrix.data[0], &modelViewMatrix.data[0], &modelViewProjection.data[0]);
        
        NSLog(@"Editing matrix");
        
        NSString * targetName = viewController.searchResult.targetName;
        if ([targetName isEqualToString:@"Teapocketpale"]) {
            selectedBox = redBox;
        } else if ([targetName isEqualToString:@"whiteTea"]) {
            selectedBox = whiteBox;
        }
        [[[[self scnView] scene] rootNode] addChildNode:selectedBox];
        if (!viewController.isAdVisible) {
            [viewController presentAdWithSearchResults];
        }
        
        [self setCameraMatrix:modelViewMatrix];
        [self.renderer renderAtTime:CFAbsoluteTimeGetCurrent() - self.startTime]; // Render objects into OpenGL context
        
        SampleApplicationUtils::checkGlError("EAGLView renderFrameVuforia");
    }
    
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    
    [self presentFramebuffer];
    
    if (state.getNumTrackableResults() > 0) {
        // we have a trackable so we stop the finder if it was on
        if ([viewController isVisualSearchOn]) {
            [viewController toggleVisualSearch];
        }
    } else {
        // we have a no trackable so we retsrat the finder if not already started
        if (! [viewController isVisualSearchOn]) {
            [viewController toggleVisualSearch];
        }
    }
    
}

- (void)configureVideoBackgroundWithViewWidth:(float)viewWidth andHeight:(float)viewHeight
{
    [sampleAppRenderer configureVideoBackgroundWithViewWidth:viewWidth andHeight:viewHeight];
}

//------------------------------------------------------------------------------
#pragma mark - Private methods

//------------------------------------------------------------------------------
#pragma mark - OpenGL ES management

- (void)initShaders
{
    shaderProgramID = [SampleApplicationShaderUtils createProgramWithVertexShaderFileName:@"Simple.vertsh"
                                                   fragmentShaderFileName:@"Simple.fragsh"];
    
    if (0 < shaderProgramID) {
        vertexHandle = glGetAttribLocation(shaderProgramID, "vertexPosition");
        normalHandle = glGetAttribLocation(shaderProgramID, "vertexNormal");
        textureCoordHandle = glGetAttribLocation(shaderProgramID, "vertexTexCoord");
        mvpMatrixHandle = glGetUniformLocation(shaderProgramID, "modelViewProjectionMatrix");
        texSampler2DHandle  = glGetUniformLocation(shaderProgramID,"texSampler2D");
    }
    else {
        NSLog(@"Could not initialise augmentation shader");
    }
}


- (void)createFramebuffer
{
    if (context) {
        // Create default framebuffer object
        glGenFramebuffers(1, &defaultFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        
        // Create colour renderbuffer and allocate backing store
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        // Allocate the renderbuffer's storage (shared with the drawable object)
        [context renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
        GLint framebufferWidth;
        GLint framebufferHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &framebufferWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &framebufferHeight);
        
        // Create the depth render buffer and allocate storage
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, framebufferWidth, framebufferHeight);
        
        // Attach colour and depth render buffers to the frame buffer
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        // Leave the colour render buffer bound so future rendering operations will act on it
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    }
}


- (void)deleteFramebuffer
{
    if (context) {
        [EAGLContext setCurrentContext:context];
        
        if (defaultFramebuffer) {
            glDeleteFramebuffers(1, &defaultFramebuffer);
            defaultFramebuffer = 0;
        }
        
        if (colorRenderbuffer) {
            glDeleteRenderbuffers(1, &colorRenderbuffer);
            colorRenderbuffer = 0;
        }
        
        if (depthRenderbuffer) {
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            depthRenderbuffer = 0;
        }
    }
}


- (void)setFramebuffer
{
    // The EAGLContext must be set for each thread that wishes to use it.  Set
    // it the first time this method is called (on the render thread)
    if (context != [EAGLContext currentContext]) {
        [EAGLContext setCurrentContext:context];
    }
    
    if (!defaultFramebuffer) {
        // Perform on the main thread to ensure safe memory allocation for the
        // shared buffer.  Block until the operation is complete to prevent
        // simultaneous access to the OpenGL context
        [self performSelectorOnMainThread:@selector(createFramebuffer) withObject:self waitUntilDone:YES];
    }
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
}


- (BOOL)presentFramebuffer
{
    // setFramebuffer must have been called before presentFramebuffer, therefore
    // we know the context is valid and has been set for this (render) thread
    
    // Bind the colour render buffer and present it
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
    return [context presentRenderbuffer:GL_RENDERBUFFER];
}



@end
