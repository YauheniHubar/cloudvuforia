//
//  Scene.h
//  VuforiaSamples
//
//  Created by Yauheni Hubar on 11/1/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SceneKit/SceneKit.h>
#import "CloudEAGLViewSceneSource.h"

@interface Scene : NSObject <CloudEAGLViewSceneSource>

@property (nonatomic, strong) SCNScene * scene;
@property (nonatomic, strong) SCNNode * cameraNode;
@property (nonatomic, strong) NSArray<SCNNode *> * boxes;

- (instancetype)initScene;
- (void)rotate:(int)i;
- (SCNScene *)sceneForEAGLView:(UIView *)view;

@end
