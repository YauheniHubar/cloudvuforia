//
//  Scene.m
//  VuforiaSamples
//
//  Created by Yauheni Hubar on 11/1/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#import "Scene.h"

@implementation Scene

- (instancetype)initScene {
    if (self = [super init]) {
        SCNCamera * c = [SCNCamera new];
//        NSMutableArray<SCNNode *> * boxesarray = [NSMutableArray arrayWithObjects:
//                                                [SCNNode nodeWithGeometry:[SCNBox boxWithWidth:1 height:0.4 / 0.79 length:0.35 / 0.79 chamferRadius:0.05]],
//                                                [SCNNode nodeWithGeometry:[SCNBox boxWithWidth:1 height:0.33 / 0.766 length:0.22 / 0.766 chamferRadius:0.05]],
//                                                nil];
//        [self setCameraNode:[SCNNode new]];
//        [self setBoxes:boxesarray];
        [self setScene:[SCNScene new]];
        [[self cameraNode] setCamera:c];
//        [c setZNear:0.1];
//        
//        //box
//        NSArray<NSString *> * imageNames = [NSArray arrayWithObjects:@"hw", @"hr", nil];
//        for (NSString * name in imageNames) {
//            SCNMaterial * mat = [SCNMaterial new];
//            [[mat diffuse] setContents:[UIImage imageNamed:[NSString stringWithFormat:@"%@1d.jpg", name]]];
//            [[[[self boxes] objectAtIndex:[imageNames indexOfObject:name]] geometry] setMaterials:[NSArray arrayWithObject:mat]];
//            for (int i = 0; i < 5; i++) {
//                SCNMaterial * mat = [SCNMaterial new];
//                [[mat diffuse] setContents:[UIImage imageNamed:[NSString stringWithFormat:@"%@%d.jpg", name, i]]];
//                NSMutableArray<SCNMaterial *> * mats = [[[[[self boxes] objectAtIndex:[imageNames indexOfObject:name]] geometry] materials] mutableCopy];
//                [mats addObject:mat];
//                [[[[self boxes] objectAtIndex:[imageNames indexOfObject:name]] geometry] setMaterials:mats];
//            }
//        }
//        
//        [[[self boxes] objectAtIndex:0] setPosition:SCNVector3Make(0, 0, -0.35 / 0.79 / 2.0)];
//        [[[self boxes] objectAtIndex:1] setPosition:SCNVector3Make(0, 0, -0.22 / 0.766 / 2.0)];
        return self;
    }
    return nil;
}

- (SCNScene *)sceneForEAGLView:(UIView *)view {
    return [self scene];
}

- (void)rotate:(int)i {
    [[[self boxes] objectAtIndex:i] setPosition:SCNVector3Make(0, 0, 0)];
    [[self cameraNode] setPosition:SCNVector3Make((float)i * 1.0, 0, 2.0)];
    [[[self boxes] objectAtIndex:(i + 1)/2] setPosition:SCNVector3Make(1000, 1000, 0)];
    [[[self boxes] objectAtIndex:i] runAction:[SCNAction repeatActionForever:[SCNAction rotateByX:0 y:1 z:0 duration:3]]];
}

@end
