//
//  SearchResult.h
//  VuforiaSamples
//
//  Created by Yauheni Hubar on 11/3/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchResult : NSObject

@property (nonatomic, copy) NSString * targetName;
@property (nonatomic, copy) NSString * uniqueTargetId;
@property (nonatomic, strong) NSNumber * targetSize;
@property (nonatomic, copy) NSString * metadata;
@property (nonatomic, strong) NSNumber * trackingRating;

@end
