//
//  AdViewController.h
//  VuforiaSamples
//
//  Created by Yauheni Hubar on 11/3/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SearchResult;

@interface AdViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *teaNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *teaPriceLabel;

@property (nonatomic, strong) SearchResult * searchResult;

@end
