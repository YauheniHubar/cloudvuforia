//
//  CloudEAGLViewSceneSource.h
//  VuforiaSamples
//
//  Created by Yauheni Hubar on 11/2/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SceneKit/SceneKit.h>

@protocol CloudEAGLViewSceneSource <NSObject>

- (SCNScene *)sceneForEAGLView:(UIView *)view;

@end
